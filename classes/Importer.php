<?php

/**
 * Created by PhpStorm.
 * User: pavelmyznikov
 * Date: 21.05.17
 * Time: 2:28
 */
class Importer
{

    static public function importDKS($from, $to, $file, $dbConnector)
    {

        $deleteQuery = 'DELETE FROM reestr_dks WHERE departure_datetime_plan BETWEEN :date_from AND :date_to';
        $deleteStmn = $dbConnector->prepare($deleteQuery);

        $deleteStmn->bindValue(':date_from', $from, SQLITE3_TEXT);
        $deleteStmn->bindValue(':date_to', $to, SQLITE3_TEXT);

        $deleteStmn->execute();

        $deleteStmn->close();

        $insertQuery = 'INSERT INTO reestr_dks VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';



        if (($fp = fopen($file, "r")) !== false) {
            $j = 0;
            while (($regel = fgetcsv($fp, 1000, ",")) !== false) {

                $j++;
                if ($j==1) continue;

                $insertPrepare = $dbConnector->prepare($insertQuery);

                $insertPrepare->bindParam(1, $regel[0], SQLITE3_TEXT);
                $insertPrepare->bindParam(2, $regel[1], SQLITE3_TEXT);
                $insertPrepare->bindParam(3, $regel[2], SQLITE3_TEXT);
                $insertPrepare->bindParam(4, $regel[3], SQLITE3_TEXT);
                $insertPrepare->bindParam(5, $regel[4], SQLITE3_TEXT);
                $insertPrepare->bindParam(6, $regel[5], SQLITE3_TEXT);
                $insertPrepare->bindParam(7, $regel[6], SQLITE3_TEXT);
                $insertPrepare->bindParam(8, $regel[7], SQLITE3_TEXT);
                $insertPrepare->bindParam(9, $regel[8], SQLITE3_TEXT);
                $insertPrepare->bindParam(10, $regel[9], SQLITE3_TEXT);
                $insertPrepare->bindParam(11, $regel[10], SQLITE3_TEXT);
                $insertPrepare->bindParam(12, $regel[11]);
                $insertPrepare->bindParam(13, $regel[12]);
                $insertPrepare->bindParam(14, $regel[13], SQLITE3_TEXT);
                $insertPrepare->bindParam(15, $regel[14], SQLITE3_TEXT);
                $insertPrepare->bindParam(16, $regel[15], SQLITE3_TEXT);
                $insertPrepare->bindParam(17, $regel[16], SQLITE3_TEXT);
                $insertPrepare->bindParam(18, $regel[17], SQLITE3_TEXT);
                $insertPrepare->bindParam(19, $regel[18]);
                $insertPrepare->bindParam(20, $regel[19]);
                $insertPrepare->bindParam(21, $regel[20], SQLITE3_TEXT);
                $insertPrepare->bindParam(22, $regel[21], SQLITE3_TEXT);
                $insertPrepare->bindParam(23, $regel[22], SQLITE3_TEXT);


                $insertPrepare->execute();
                $insertPrepare->close();
            }

        }


        return true;
    }
}