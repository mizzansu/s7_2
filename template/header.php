<?php
/**
 * Created by PhpStorm.
 * User: pavelmyznikov
 * Date: 20.05.17
 * Time: 17:06
 */

error_reporting(0);
ini_set('display_errors', 0);

$dbConnection = new SQLite3($_SERVER['DOCUMENT_ROOT'].'/s7');

?>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Проверка бортового питания</title>

        <!-- Bootstrap core CSS -->
        <link href="/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/starter-template.css" rel="stylesheet">
        <link href="/sticky-footer.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="/assets/js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="icon" href="favicon.ico">
    </head>
    <body>
        <!--<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #bed600;border-color: #f2a900">-->
        <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #bed600;border-color: #bed600">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <!--<span class="sr-only">Toggle navigation</span>-->
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="color: white">S7 финансы</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!--<li class="active"><a href="#">Home</a></li>-->
                        <li><a href="/" style="color: white">Главная</a></li>
                        <li><a href="/input_files/" style="color: white">Шаблоны входных файлов</a></li>
                        <li><a href="/analytics/" style="color: white">Аналитика</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">

