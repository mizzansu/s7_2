    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>

    <script>
        $(function(){
            $('#import-form').submit(function(){
                $('#import-info').text('Загружаем данные...');
                $('#import-submit').replaceWith('<div class="s7-loader"></div>');
                $.post({
                    url: "/import-file.php",
                    data: {
                        file: '<?=$_SERVER['DOCUMENT_ROOT']?>/data/reest_dks_3_decade_jan.csv',
                        date_from: '2017-01-21 00:00:00',
                        date_to: '2017-01-31 23:59:59'
                    },
                    success: function($res){
                        if ($res == 'ok') {
                            $('#import-info').text('Делаем проверку данных...');
                            setTimeout(function(){$('#import-info').text('Сохраняем отчёт...')}, 1500);
                            $.get({
                                url: "/cross-check.php",
                                data: {
                                    date_from: '2017-01-21',
                                    date_to: '2017-01-31'
                                },
                                success: function($res){
                                    if ($res=="ok") {
                                        $('.s7-loader').remove();
                                        $('#import-info').html($('<a></a>').text('Отчёт готов. Пройдите по ссылке').attr('href','/check/?date_from=2017-01-21&date_to=2017-01-31'))
                                    }
                                }
                            })
                        }
                    }
                });
                return false;
            });
        });
    </script>
    <footer class="footer">
        <div class="container">
            <p class="text-muted">Команда 13: Мызников Павел | Забайкин Алексей | Токмашев Роман || г.Новосибирск</p>
        </div>
    </footer>
    </body>
</html>
