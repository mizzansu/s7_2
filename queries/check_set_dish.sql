SELECT
  t.flight_number 'Flight number',
  t.date_plan_loc 'date_plan',
  t.set_crew_to_be,
  dishes_to_be.dishes 'k_dishes_to_be',
  dishes_to_be.sum 'k_dishes_to_be_sum',
  dishes_crew_fact.dishes 'k_dishes_fact',
  dishes_crew_fact.sum 'k_dishes_fact_sum',
  ROUND(coalesce(dishes_crew_fact.sum, 0) - coalesce(dishes_to_be.sum, 0),2) 'k_delta',
  set_business_to_be,
  meals_business_to_be.dishes 'c_dishes_to_be',
  meals_business_to_be.sum_per_full_dish 'c_dishes_to_be_sum',
  meals_business_fact.dishes 'c_dishes_fact',
  meals_business_fact.sum 'c_dishes_sum',
  ROUND(coalesce(meals_business_fact.sum, 0) - coalesce(meals_business_to_be.sum_per_full_dish, 0),2) 'c_delta'
FROM
  (SELECT
  flight_schedule.flight_number,
  date_plan_loc,
  strftime('%W', date_plan_loc) % 2 'week_odd',
  CASE WHEN (strftime('%W', date_plan_loc) % 2) = 0 THEN meal_schedule.c_even ELSE meal_schedule.c_odd END 'set_business_to_be',
  CASE WHEN (strftime('%W', date_plan_loc) % 2) = 0 THEN meal_schedule.k_even ELSE meal_schedule.k_odd END 'set_crew_to_be'
FROM
  flight_schedule
LEFT JOIN meal_schedule ON flight_schedule.flight_number = CAST(meal_schedule.flight_number as integer)
WHERE date_plan_loc BETWEEN :date_from AND :date_to) t
LEFT JOIN (
  SELECT
    title 'meal',
    group_concat(dish) 'dishes',
    SUM(k_dishes.price) 'sum'
  FROM loading_matrix_ration
  LEFT JOIN k_dishes ON loading_matrix_ration.dish = k_dishes.dish_code
  GROUP BY title
          ) dishes_to_be ON t.set_crew_to_be = dishes_to_be.meal
LEFT JOIN (
    SELECT
      flight_number,
      date(departure_datetime_plan) 'date_fact',
      group_concat(classification_code) 'dishes',
      SUM(k_dishes.price) 'sum'
    FROM
      reestr_dks
    LEFT JOIN k_dishes ON reestr_dks.classification_code = k_dishes.dish_code
    WHERE classification_type='Блюдо' and flight_class = 'Экипаж ВС'
    GROUP BY
      flight_number,
      date(departure_datetime_plan)
    ) dishes_crew_fact ON t.date_plan_loc = dishes_crew_fact.date_fact AND (dishes_crew_fact.flight_number LIKE 'С7' || t.flight_number OR dishes_crew_fact.flight_number LIKE 'ГЛ' || t.flight_number)
LEFT JOIN (
    SELECT
      flight_number,
      date_fact,
      group_concat(meal_code) 'dishes',
      ROUND(SUM(meal_cost),2) 'sum'
    FROM(
      SELECT
        c_dishes_fact.flight_number,
        c_dishes_fact.date_fact,
        c_meals_fact.meal_code,
        round(SUM(sum_per_dish),2) 'meal_cost',
        group_concat(c_meals_fact.dish_code, ',') 'dishes'
      FROM
        (SELECT
              flight_number,
              date(departure_datetime_plan) 'date_fact',
              classification_code 'dish_code',
              sum / amount 'sum_per_dish'
            FROM reestr_dks
            WHERE classification_type='Блюдо' and flight_class = 'Бизнес'
            ) c_dishes_fact
        JOIN (
            SELECT
              meal_code,
              dish_code
            FROM c_meals
            ) c_meals_fact ON c_meals_fact.dish_code = c_dishes_fact.dish_code
      GROUP BY c_dishes_fact.flight_number, c_dishes_fact.date_fact, c_meals_fact.meal_code
      ORDER BY c_dishes_fact.flight_number, c_dishes_fact.date_fact, c_dishes_fact.dish_code DESC
    )
    GROUP BY flight_number, date_fact
) meals_business_fact ON t.date_plan_loc = meals_business_fact.date_fact AND (meals_business_fact.flight_number LIKE 'С7' || t.flight_number OR meals_business_fact.flight_number LIKE 'ГЛ' || t.flight_number)
LEFT JOIN (
    SELECT
      iata,
      group_concat(meal_code) 'dishes',
      round(sum(price / dishout_part * 100),2) 'sum_per_full_dish'
    FROM (
      SELECT
        iata,
        meal_code,
        price,
        dishout_part
      FROM
        ration_c_content
      ORDER BY iata, meal_code DESC
    )
    GROUP BY iata
) meals_business_to_be ON t.set_business_to_be = meals_business_to_be.iata


