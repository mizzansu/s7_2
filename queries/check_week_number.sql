SELECT
  t.flight_number 'Flight number',
  t.date_plan_loc 'date_plan',
  t.set_econom_to_be 'set_econom_to_be',
  t.set_econom_fact 'set_econom_fact',
  ration_econom_fact.price - ration_econom_plan.price 'econom_delta',
  t.set_business_to_be 'set_business_to_be',
  t.set_business_fact 'set_business_fact',
  (ration_business_fact.price_wo_vat + ration_business_fact.price_tray) - (ration_business_plan.price_wo_vat + ration_business_plan.price_tray) 'business_delta',
  t.set_crew_to_be 'set_crew_to_be',
  t.set_crew_fact 'set_crew_fact',
  ration_crew_fact.price - ration_crew_plan.price 'crew_delta'
FROM
(SELECT
  y.flight_number 'flight_number',
  date_plan_loc,
  strftime('%W', date_plan_loc) % 2 'week_odd',
  CASE WHEN (strftime('%W', date_plan_loc) % 2) = 0 THEN meal_schedule.y_even ELSE meal_schedule.y_odd END 'set_econom_to_be',
  y.iata_code 'set_econom_fact',
  CASE WHEN (strftime('%W', date_plan_loc) % 2) = 0 THEN meal_schedule.c_even ELSE meal_schedule.c_odd END 'set_business_to_be',
  c.iata_code 'set_business_fact',
  CASE WHEN (strftime('%W', date_plan_loc) % 2) = 0 THEN meal_schedule.k_even ELSE meal_schedule.k_odd END 'set_crew_to_be',
  k.iata_code 'set_crew_fact'
FROM
  flight_schedule
LEFT JOIN meal_schedule ON flight_schedule.flight_number = CAST(meal_schedule.flight_number as integer)
LEFT JOIN (
    SELECT
      iata_code,
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND
      flight_class = 'Эконом'
    GROUP BY flight_number, DATE(departure_datetime_plan)
) y ON flight_schedule.date_plan_loc = y.date_plan AND (y.flight_number LIKE 'С7' || flight_schedule.flight_number OR y.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
  LEFT JOIN (
    SELECT
      iata_code,
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND
      flight_class = 'Бизнес'
    GROUP BY flight_number, DATE(departure_datetime_plan)
) c ON flight_schedule.date_plan_loc = c.date_plan AND (c.flight_number LIKE 'С7' || flight_schedule.flight_number OR c.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
LEFT JOIN (
    SELECT
      iata_code,
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND
      flight_class = 'Экипаж ВС'
    GROUP BY flight_number, DATE(departure_datetime_plan)
) k ON flight_schedule.date_plan_loc = k.date_plan AND (k.flight_number LIKE 'С7' || flight_schedule.flight_number OR k.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
WHERE date_plan_loc >= '2017-01-01' AND y.flight_number IS NOT NULL) t
LEFT JOIN ration 'ration_econom_fact' ON 'C7/GH' || t.set_econom_fact = ration_econom_fact.code_iata
LEFT JOIN ration 'ration_econom_plan' ON 'C7/GH' || t.set_econom_to_be = ration_econom_plan.code_iata
LEFT JOIN ration_c 'ration_business_fact' ON t.set_business_fact = ration_business_fact.iata
LEFT JOIN ration_c 'ration_business_plan' ON t.set_business_to_be = ration_business_plan.iata
LEFT JOIN ration 'ration_crew_fact' ON 'C7/GH' || replace(t.set_crew_fact, 'К', 'K') = ration_crew_fact.code_iata
LEFT JOIN ration 'ration_crew_plan' ON 'C7/GH' || replace(t.set_crew_to_be, 'К', 'K') = ration_crew_plan.code_iata
WHERE t.date_plan_loc BETWEEN :date_from AND :date_to