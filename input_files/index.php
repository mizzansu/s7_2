<?php
/**
 * Created by PhpStorm.
 * User: pavelmyznikov
 * Date: 20.05.17
 * Time: 18:29
 */

?>

<?php include('../template/header.php'); ?>

<h1>Шаблоны входных файлов</h1>
<div class="well">Система гарантирует работу с входными данными в прилагаемых форматах. Выберите нужный файл и отправьте ответственным лицам.
</div>
<div class="row">
    <table class="table">
        <thead>
        <tr>
            <th>
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                Ответственное лицо
            </th>
            <th>
                <span class="glyphicon glyphicon-download" aria-hidden="true"></span>
                Файл
            </th>
            <th>
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                E-mail
            </th>
            <th><span class="glyphicon glyphicon-send" aria-hidden="true"></span></th>
        </tr>
        </thead>
        <?php
            $all = array(
                ['DKS (Домодедово)', 'reestr_DKS.csv', 'schedule_info@dks.com'],
                ['TMS (Толмачево)', 'reestr_TMS.csv', 'schedule_info@tolmachevo.com'],
                ['S7: отдел поставщиков', 'matrix.csv', 'afedorov@s7.com'],
                ['S7: отдел загрузки', 'zagruzki.csv', 'ipitkina@s7.com'],
                ['S7: отдел составления расписаний', 'schedule.csv', 'gveleriev@s7.com'],
            );
        ?>
        <?php foreach ($all as $a){ ?>
        <tr>
            <th><?php echo $a[0] ?></th>
            <th><a href="template_for_partner.csv"><?php echo $a[1] ?></a></th>
            <th>
                <form>
                    <div class="form-group">
                        <input class="form-control" type="email" value="<?php echo $a[2] ?>" />
                    </div>
                </form>
            </th>
            <th><button type="button" class="btn btn-default">Отправить</button></th>
        </tr>
        <?php }?>
    </table>
</div>


<?php include('../template/footer.php')?>