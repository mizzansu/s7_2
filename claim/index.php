<?php


$dbConnection = new SQLite3($_SERVER['DOCUMENT_ROOT'].'/s7');

$query = 'SELECT * FROM analysis'. str_replace("-", "", $_GET['date_from']).'_'. str_replace("-", "", $_GET['date_to']);

$result = $dbConnection->query($query);

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=claim'.str_replace("-", "", $_GET['date_from']).'_'. str_replace("-", "", $_GET['date_to']).'.csv');

$output = fopen('php://output', 'w');

$row_titles = [
    "flight_number",
    "aircraft",
    "date_plan",
    "date_fact",
    "econom_amount_passengers",
    "econom_sets",
    "econom_set_to_be",
    "econom_set_fact",
    "econom_set_delta",
    "econom_verdict",
    "econom_errors",
    "business_amount_passengers",
    "business_sets",
    "business_set_to_be",
    "business_set_fact",
    "business_set_delta" ,
    "business_dishes_to_be" ,
    "business_dishes_fact" ,
    "business_dishes_to_be_sum" ,
    "business_dishes_fact_sum" ,
    "business_dishes_delta" ,
    "business_verdict" ,
    "business_errors" ,
    "crew_amount_passengers" ,
    "crew_sets" ,
    "crew_set_to_be" ,
    "crew_set_fact" ,
    "crew_set_delta" ,
    "crew_dishes_to_be" ,
    "crew_dishes_fact" ,
    "crew_dishes_to_be_sum",
    "crew_dishes_fact_sum",
    "crew_dishes_delta",
    "crew_verdict",
    "crew_errors"
    ];
fputcsv($output, $row_titles);

while ($row = $result->fetchArray()) {
    fputcsv($output, $row);

}